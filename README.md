---

# GitLab CI/CD Web Application Deployment

## Table of Contents

- [GitLab Repository Setup](#gitlab-repository-setup)
- [Sample Web Application](#sample-web-application)
- [Configure GitLab CI/CD Pipeline](#configure-gitlab-cicd-pipeline)
- [Deployment](#deployment)

---

## GitLab Repository Setup

### 1. Create a new GitLab repository

- Using [GitLab](https://gitlab.com/) I've created a new public repository for this assignment.

### 2. Clone the repository to your local machine

- After that I've cloned that repo to my local machine using following command...
```bash
git clone https://gitlab.com/your-username/your-repository.git
cd your-repository
```

---

## Sample Web Application

### 1. Create a simple web application

- Node.js framework is used to create a web application..
- Implemented basic functionalities like I've created a user registration form.
- Respective pdf contains the screenshots of project running sucessfully on localhost.


---

## Configure GitLab CI/CD Pipeline

### 1. Created a `.gitlab-ci.yml` file

- In the root of my repository, I've created a `.gitlab-ci.yml` file.

### 2. Defined CI/CD pipeline stages and jobs

```yaml
stages:
    - build_stage
    - deploy_stage

build:
    stage: build_stage
    image: node
    script:
        - npm install
    artifacts:
        paths:
            - node_modules
            - package-lock.json

deploy:
    stage: deploy_stage
    image: node
    script:
        - node app.js /dev/null 2>&1 &

```

---

## Deployment

### 1. Configured deployment job

- Adjusted the deployment job in `.gitlab-ci.yml` to automatically deploy the application to the staging environment upon successful testing.


### 2. Connected GitLab repository to Jenkins server

- At last, Jenkins is connected to my GitLab repository, and the Jenkins server successfully builds the application.
- Application build successful
- console is running successfully as shown in shared pdf with this repo.

---

---
Thank You!
---
